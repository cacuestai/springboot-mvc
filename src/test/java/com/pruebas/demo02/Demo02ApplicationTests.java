package com.pruebas.demo02;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.pruebas.demo02.model.Usuario;
import com.pruebas.demo02.repository.IUsuario;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
class Demo02ApplicationTests {

	@Autowired
	private IUsuario repo;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Test
	void contextLoads() {
		Usuario usuario = new Usuario();
		usuario.setId(4);
		usuario.setNombre("cacuestai");
		usuario.setClave(encoder.encode("123"));
		Usuario nuevoUsuario = repo.save(usuario);
		assertTrue(nuevoUsuario.getClave().equalsIgnoreCase(usuario.getClave()));
	}

}
