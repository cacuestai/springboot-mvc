package com.pruebas.demo02.repository;

import com.pruebas.demo02.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUsuario extends JpaRepository<Usuario, Long> {

    Usuario findByNombre(String nombre);

}
