package com.pruebas.demo02.repository;

import com.pruebas.demo02.model.Persona;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IPersona extends JpaRepository<Persona, Long>{
    
}
