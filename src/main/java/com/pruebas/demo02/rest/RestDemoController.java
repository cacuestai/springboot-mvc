package com.pruebas.demo02.rest;

import java.util.List;

import com.pruebas.demo02.model.Persona;
import com.pruebas.demo02.repository.IPersona;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// Ver Modelo de Madurez de Richardson

@RestController
@RequestMapping("/personas")
public class RestDemoController {

   @Autowired
   private IPersona repo;

   /**
    * Usa get
    */
   @GetMapping
   public List<Persona> listar() {
      return repo.findAll();
   }

   /**
    * Para pruebas usar PostMan y enviar datos en formato JSON
    * @param p
    */
   @PostMapping
   public void insertar(@RequestBody Persona p) {
      repo.save(p);
   }
   
   /**
    * Para pruebas usar PostMan y enviar datos en formato JSON
    */
   @PutMapping
   public void modificar(@RequestBody Persona p) {
      repo.save(p);
   }
   
   /**
    * Ejemplo de la llamada:
    * http://localhost:8080/personas/5
    * @param id
    */
   @DeleteMapping(value = "/{id}")
   public void eliminar(@PathVariable("id") Long id) {
      repo.deleteById(id);
   }
   
}
