package com.pruebas.demo02.controller;

import com.pruebas.demo02.model.Persona;
import com.pruebas.demo02.repository.IPersona;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * http://localhost:8080/crear (usa el parámetro enviado por get)
 */

@Controller
public class DemoController {

	@Autowired
	private IPersona repo;

	
	@GetMapping("/saludo") // petición get con alias "nombre" es el parámetro que llega por get y que es recibido por String nombre
	public String greeting(@RequestParam(name = "nombre", required = false, defaultValue = "Carlos Cuesta") String nombre, Model model) {
		model.addAttribute("nombre", nombre); // envía al html un atributo "nombre" con el nombre recibido
		return "saludo"; // página html disponible en ./src/main/resources/templates/
	}

	@GetMapping("/crear")
	public String crear(@RequestParam(name = "nombre", required = false, defaultValue = "Carlos Cuesta") String nombre, Model model) {
		Persona p = new Persona();
		p.setNombre(nombre);
		repo.save(p);
		model.addAttribute("nombre", nombre); // envía al html un atributo "nombre" con el nombre recibido
		return "saludo"; // página html disponible en ./src/main/resources/templates/
	}

	@GetMapping("/listar")
	public String greeting(Model model) {
		model.addAttribute("personas", repo.findAll());
		return "saludo";
	}

}
