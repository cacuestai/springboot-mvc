package com.pruebas.demo02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring MVC
 * Basado en https://spring.io/guides/gs/serving-web-content/
 * Ver en pom.xlm que se usan las dependencias Web y Thymeleaf
 * Ver en ./src/main/resources/application.properties el usuario y la contraseña
 * Ejemplos de llamadas: 
 *  http://localhost:8080/saludo  (usa el parámetro por defecto)
 *  http://localhost:8080/saludo?nombre=Caliche  (usa el parámetro enviado por get)
 */

@SpringBootApplication
public class Demo02Application {

	public static void main(String[] args) {
		SpringApplication.run(Demo02Application.class, args);
	}

}
