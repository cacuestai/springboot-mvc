package com.pruebas.demo02.service;

import java.util.ArrayList;
import java.util.List;

import com.pruebas.demo02.model.Usuario;
import com.pruebas.demo02.repository.IUsuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

   @Autowired
   private IUsuario repo;

   @Override
   public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
      Usuario u = repo.findByNombre(username);
      List<GrantedAuthority> roles = new ArrayList<>();
      roles.add(new SimpleGrantedAuthority("ADMIN"));
      return new User(u.getNombre(), u.getClave(), roles);
   }

}
